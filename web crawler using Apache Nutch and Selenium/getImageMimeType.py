__author__ = 'CharanShampur'
import glob
import sys
import nutchpy
# This program extracts the different mime types and the URLS of images from the crawlDB of Nutch
# The extracted data are written to two output files mimeType.txt and imageUrls.txt created in the current working dir

if(len(sys.argv)<3):
    print "Execution = $ python getImageMimeType.py <Path to crawldb dataset of nutch> <mimType output file>"
    exit()

crawl_path = sys.argv[1]


# crawl_path = "/Users/charanshampur/nutch/crawl_data3/crawldb"
imageMimeFile = open(sys.argv[2],'w')
imageUrlsFile = open('imageUrls.txt','w')

seq_reader = nutchpy.sequence_reader
#500 records are read each time when reading the crawldb dataset
x=0
j=500
# content type that you are searching for
matchStr = 'Content-Type=image/'
imageTypeList = []
# searches for all the data file inside crawl directory
crawlFile = glob.glob(crawl_path+"/*/*/data")
if(len(crawlFile)==0):
    print "Incorect path"
    exit()

imageCountDict = {}

for node_path in crawlFile:
    while True:
        listCrawl = seq_reader.slice(x,j,node_path)
        if(len(listCrawl)==0):
            break
        strOut = ''
        for urlData in listCrawl:
            if matchStr in urlData[1]:
                strOut=''
                index = urlData[1].find(matchStr)
                index+=len(matchStr)
                for i in range(index,len(urlData[1])):
                    if urlData[1][i] == '\n':
                        break
                    else:
                        strOut+=urlData[1][i]
                try:
                    imageUrlsFile.write(urlData[0].encode('ascii','ignore')+"        "+ matchStr + strOut + "\n")
                except:
                    pass
                if strOut not in imageTypeList:
                    imageTypeList.append(strOut)
                    imageMimeFile.write("image/"+str(strOut)+"\n")
                    imageCountDict[strOut]=1
                    del strOut
                else:
                    imageCountDict[strOut]+=1
        x+=500
        j+=500
        if len(listCrawl)!=500:
            break

imageMimeFile.write("\n\n*************  Statistics  *************" + "\n")
for k,v in imageCountDict.items():
    try:
        imageMimeFile.write("No of "+str(k)+" images = " + str(v) + "\n")
    except:
        continue
imageMimeFile.write("****************************************")
imageMimeFile.close()
imageUrlsFile.close()