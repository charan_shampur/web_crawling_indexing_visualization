import os
import sys
import nutchpy
import hashlib
import json
import glob

# if the path is not provided in the command line argument
if len(sys.argv) < 2:
    print "python findExactDuplicates.py /path/to/segments/directory/"
    sys.exit()

givenPath = sys.argv[1]

def getMD5Checksum(imageinfo):
    # find MD5 hash of the image info

    image_md5 = hashlib.md5(imageinfo).hexdigest()
    return image_md5

def initializeBuckets(imageList):
    # any collisions are added in the same bucket.
    # all urls in the same bucket will be exact duplicates
    buckets = {}
    for image in imageList:
        image_hash = getMD5Checksum(image["extractedData"])

        if image_hash not in buckets.keys():
            buckets[image_hash] = []
            buckets[image_hash].append(image["url"])
        else:
            buckets[image_hash].append(image["url"])

    return buckets

def getMetadata(metaStr,metaData,altData):
    #extrMeta = metaData
    if (metaStr.find(metaData)<0):
        metaData=altData
    if (metaStr.find(metaData)>0):
        indexStart = metaStr.find(metaData)
        indexEnd = metaStr.find(' ',indexStart+len(metaData))
        if(metaStr[indexEnd-1]==','):
            extrMeta = metaStr[indexStart+len(metaData):indexEnd-1]
        else:
            extrMeta = metaStr[indexStart+len(metaData):indexEnd]
    else:
        extrMeta="null"
    return extrMeta

def extractImgInfo(imgMetadata, imgText):
    imgInfo = {}
    imgInfo["imageText"] = imgText.encode('ascii', 'replace')
    try:
        imgInfo["url"] = imgMetadata[0]
        #imgInfo["filename"] = imgMetadata[0].split('/').[-1]
    except:
        return False

    for entry in imgMetadata[1]:
        if entry.find('Metadata'):
            imgInfo["width"] = getMetadata(imgMetadata[1].lower(), "width=", "null").encode('ascii', 'replace')
            imgInfo["height"] = getMetadata(imgMetadata[1].lower(), "height=", "null").encode('ascii', 'replace')
            imgInfo["contentLength"] = getMetadata(imgMetadata[1].lower(), "content-length=", "null").encode('ascii', 'replace')
            imgInfo["contentType"] = getMetadata(imgMetadata[1].lower(), "content-type=", "null").encode('ascii', 'replace')
            imgInfo["fileSize"] = getMetadata(imgMetadata[1].lower(), "file size=", "null").encode('ascii', 'replace')

    imgInfo["extractedData"] = imgInfo["width"] + imgInfo["height"] + imgInfo["contentLength"] + \
                                    imgInfo["contentType"] + imgInfo["fileSize"] + imgInfo["imageText"]
    imgInfo["extractedData"] = imgInfo["extractedData"].lower()
    return imgInfo

# if path provided exists
if os.path.isdir(givenPath):
    parseDataFiles = glob.glob(givenPath + "/*/parse_data/*/data")
    parseTextFiles = glob.glob(givenPath + "/*/parse_text/*/data")
    # if the file 'data' is present
    seq_reader = nutchpy.sequence_reader
    imageList = []

    x = 0
    for m in range(0,len(parseDataFiles)):
        dataPath = parseDataFiles[m]
        textPath = parseTextFiles[m]
        listData = seq_reader.read(dataPath)
        listText = seq_reader.read(textPath)

        print len(listData)

        i = 0
        for i in range(0, len(listData)):
            if(len(listData[i][1])>2000):
                continue

            image_data = listData[i]
            image_text = listText[i][1]
            params = image_data[1].split("\n")

            # in the params look for Content-Type
            for item in params:
                if "Content-Type=image" in item:
                    retval = extractImgInfo(image_data, image_text)
                    if retval != 0:
                        imageList.append(retval)

            # if len(listData) < 500 or len(listText) < 500:
            #     break
            # x += 500

        # else:
        #     print "\nThe path " + dataPath + " not found. Please check and retry\n"
        #     sys.exit()

    buckets = initializeBuckets(imageList)
    for key, value in buckets.items():
        if len(value) > 1:
            print "The URLs below have exact same images: \n"
            for url in value:
                print url + '\n'

#if the path provided in the command line argument doesnt exist
else:
    print "\n" + mimePath + " is not a valid path. Please check and retry\n"
