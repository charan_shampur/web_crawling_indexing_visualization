__author__ = 'charanshampur'
import json
inputJsonFile = open("/Users/charanshampur/Sites/MyHtml/circle.json","r")
#flareFile=open("/Users/charanshampur/solr/lucene_solr_4_10/solr/example/solr-webapp/webapp/MyHtml/flare.json","w")
flareFile=open("flare.json","w")
jsonStr=inputJsonFile.readline()
gunCollec={}
rawJSON = json.loads(jsonStr);

for item in rawJSON:
    if "Gun_Types_Sold" in item:
        url=item["id"];
        domBeg=url.find("//")+2
        domEnd=url.find("/",domBeg)
        domain=url[domBeg:domEnd]
        for gunType in item["Gun_Types_Sold"]:
            if gunType not in gunCollec:
                gunCollec[gunType]={domain:1}
            else:
                domainList=gunCollec[gunType]
                if domain not in domainList:
                    gunCollec[gunType][domain]=1
                else:
                    gunCollec[gunType][domain]+=1
                    

flare={"name": "flare","children":[]}
interMedList=[]
interMedDict={}
for gun in gunCollec:
    interMedDict={}
    interMedDict["name"]=gun
    children=gunCollec[gun]
    childDict={}
    childList=[]
    for child in children:
        childDict["name"]=child+" Ad_Count = "+str(children[child])
        childDict["size"]=children[child]+20
        childList.append(dict(childDict))
    interMedDict["children"]=childList
    interMedList.append(dict(interMedDict))

flare["children"]=interMedList
jsonarray = json.dumps(flare,indent=4)
flareFile.write(jsonarray)
flareFile.close()
inputJsonFile.close()
print "success"