__author__ = 'natraj'
import json
from collections import defaultdict

months=["January","February","March","April","May","June","July","August","September","October","November","December"]
with open('/Users/charanshampur/Sites/MyHtml/circle.json') as f:
    jsonStr=f.readline()
    json_data = json.loads(jsonStr)
    dict1={}
    for eachjson in json_data:
        try:
            dict1[str(eachjson['event_timestamp'])] = eachjson['id']
        except KeyError:
            continue
    array1=[]
    for val in dict1.keys():
        if val[0:4] not in array1:
            array1.append(val[0:4])
    d=defaultdict(dict)
    for val1 in array1:
        dict2=defaultdict(list)
        for val2 in dict1.keys():
            if val1 in val2:
                dict2[val2[5:7]].append(str(dict1[val2]))
        d[val1]=dict2
    res2=[]
    for val in d.keys():
        d1={}
        d1['name']=val
        res1=[]
        for val1 in d[val].keys():
            d2={}
            d2['name']=months[int(val1)-1]
            res=[]
            for val3 in d[val][val1]:
                d3={}
                d3['name']=val3
                d3['size']=133
                res.append(d3)
            d2['children']=res
            res1.append(d2)
        d1['children']=res1
        res2.append(d1)
    d4={}
    d4['name']='DateTime'
    d4['children']=res2
    #with open('/Users/charanshampur/solr/lucene_solr_4_10/solr/example/solr-webapp/webapp/MyHtml/output.json','w') as sp:
    #   json.dump(d4, sp, indent=4)
    with open('output.json','w') as sp:
        json.dump(d4, sp, indent=4)
print "success"