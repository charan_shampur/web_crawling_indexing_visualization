__author__ = 'charanshampur'
import json
import string
#freqListFile = open("/Users/charanshampur/solr/lucene_solr_4_10/solr/example/solr-webapp/webapp/MyHtml/freqList.json","w")
freqListFile = open("freqList.json","w")
inputJsonFile = open("/Users/charanshampur/Sites/MyHtml/circle.json","r")
removeWords=["FOR","LOGIN","SALE","NEW","FREE","``","BUY","SYSTEM","WANT","REPORT","WITHIN","S","...","TO","SAN","P","W/","ALL","'S","W","M","PAGE","ITEMS"]
jsonStr=inputJsonFile.readline()
#print "NLTK succesfully loaded<br>"
rawJSON = json.loads(jsonStr)
#print "Json succesfully loaded"
wordCloud={}
for item in rawJSON:
    if "title" in item:
        text = item["title"][0]
        try:
            tokens=(str(text).split())
        except:
            continue
        tokens=[w for w in tokens if w not in string.punctuation]
        for token in tokens:
            if token.upper() not in removeWords and not token.isdigit():
                if token.upper() not in wordCloud:
                    wordCloud[token.upper()]=1
                else:
                    wordCloud[token.upper()]+=1

wordCloud=sorted(wordCloud.items(), key=lambda x: x[1],reverse=True)
wordJson=[]
wordDict={}
for i in range(0,120):
    wordDict["text"]=wordCloud[i][0]
    if int(wordCloud[i][1])>80:
        normalizedSize=80
    else:
        normalizedSize=wordCloud[i][1]
    wordDict["size"]=normalizedSize
    wordJson.append(dict(wordDict))

jsonarray = json.dumps(wordJson,indent=4)
freqListFile.write(jsonarray)
freqListFile.close()
print "success"