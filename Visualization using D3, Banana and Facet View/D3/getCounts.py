import json, sys
import pprint

with open('/Users/charanshampur/Sites/MyHtml/circle.json') as jsonFile:
    rawJSON = json.load(jsonFile)

#rawJSON = rawJSON['response']['docs']

def getYear(date):
    year = date[0:4]
    return year

def getMonth(date):
    month = date[5:7]
    return month

def getQuarter(month):
    try:
        month = int(month)
    except:
        return "1-Jan-"

    if month > 0 and month < 4:
        return "1-Jan-"
    elif month > 3 and month < 7:
        return "1-Apr-"
    elif month > 6 and month < 10:
        return "1-Jul-"
    else:
        return "1-Oct-"

def writeTsv(tsvHolder):
    with open("data.tsv", "w") as tsv:
        tsv.write("date\tclose\n")
        for key, value in sorted(tsvHolder.items()):
            tsv.write(key.encode('ascii', 'ignore') + '\t' + str(value) + '\n')

tsvHolder = {}

for i, info in enumerate(rawJSON):
    try:
        if len(info['event_timestamp']) > 7:
            year = getYear(info['event_timestamp'])
            month = getMonth(info['event_timestamp'])
            quarter = getQuarter(month)
            try:
                tsvHolder[quarter + year[2:4]] += 1
            except:
                tsvHolder[quarter + year[2:4]] = 1
            
    except:
        pass

writeTsv(tsvHolder)