CSCI-572 Assignment-3 Data Visualization using Banana, Facet View and D3
Steps for setting up the application
a) start the solr server on port 8983 
b) start the httpd cgi server on port 8080
c) Place the d3 folder inside the CGI server container
d) Place Banana and Facet view inside solr/example/solr-webapp/webapp folder
e) The D3 dashboard configuration file can be laoded from .Banana default configuration can be loaded by clicking on configure from the top right corner of the dashboard.
g) The solr endpoint is configured to collection1.
h) Open the d3.html file which loads the application like given below-
	http://localhost:8080/~username/d3/d3.html
	
Link to video demonstrating the answers to challenge questions : https://youtu.be/bFpqyK34bhU
