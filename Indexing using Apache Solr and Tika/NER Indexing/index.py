__author__ = 'charanshampur'
#!/Library/Framework/Python.framework/Versions/2.7/bin/python2.7
import sys
import glob
import solr
import nutchpy
import tika
import re
import parsedatetime as pdt
from tika.parser import _parse
from tika.tika import callServer, ServerEndpoint
from tika import parser
from nltk.tag.stanford import StanfordNERTagger
tika.initVM()
if(len(sys.argv)<4):
    print "execution : python index.py <path to nutch crawl_data directory> <path to stanford-ner.jar> <path to english.all.3class.distsim.crf.ser.gz>"
    exit()

c = pdt.Constants()
p = pdt.Calendar(c)
# initialization of Stanford NER Tagger for extracting person names and location
try:
    st=StanfordNERTagger(sys.argv[3],sys.argv[2])
except:
    print "Unable to find stanford-ner.jar and english.all.3class.distsim.crf.ser.gz."
    print "Please provide a valid path"
    exit()


metaIndexData=['Optional_NAME1','Optional_LATITUDE1','Optional_LONGITUDE1','Geographic_NAME','Geographic_LONGITUDE','Geographic_LATITUDE','description',
               'title','content_type','resourcename','keywords','Ad_Creation_Date']

# function for selecting the required metadata from geo-topic parser
def getGeoTags(geoTags):
    gtag={}
    for k,v in geoTags.items():
        if re.match(r'(Optional)+|(Geographic)+',k,re.M):
            gtag[k]=v
    return gtag

# function for formatting metadata extracted from tika-ocr
def formatIndex(docMetaData):
    if "Content-Type" in docMetaData:
        docMetaData["content_type"]=docMetaData["Content-Type"]
        del docMetaData["Content-Type"]
    if "resourceName" in docMetaData:
        docMetaData["resourcename"]=docMetaData["resourceName"]
        del docMetaData["resourceName"]
    for k,v in docMetaData.items():
        if k not in metaIndexData:
            del docMetaData[k]
    return docMetaData

# function for selecting metadata extracted from ctakes
def process_ctakes(ctakesMeta):
    ctakesM={}
    if "ctakes:RangeAnnotation" in ctakesMeta:
        ctakesM["ctakes:RangeAnnotation"]=ctakesMeta["ctakes:RangeAnnotation"]
    if "ctakes:DateAnnotation" in ctakesMeta:
        ctakesM["ctakes:DateAnnotation"]=ctakesMeta["ctakes:DateAnnotation"]
    if  "ctakes:MeasurementAnnotation" in ctakesMeta:
        ctakesM["ctakes:MeasurementAnnotation"]=ctakesMeta["ctakes:MeasurementAnnotation"]
    return ctakesM

# sequence reader for reading the contents from crawled segments
seq_reader = nutchpy.sequence_reader
try:
    filesList=glob.glob(sys.argv[1]+"/segments/*/content/*/data")
except:
    print "unable to open nutch segments data, please correct the file path. eg: nutch/crawl_data/"
    exit()

# establishing a connection to solr server
solrDict={}
try:
    s = solr.Solr("http://localhost:8983/solr")
except:
    print "unable to make a connection to the solr server"
    exit()

# main function where calls are made to tika-ocr/geotopic server and tika-ctakes server
# geo topic parser runs on port 9998 and ctakes runs on port 9090
x=0
y=100
for file in filesList:
    while True:
        listCrawl = seq_reader.slice(x,x+y,file)
        for urls in listCrawl:
            url_id = urls[0]
            print url_id
            url_Contents = urls[1]
            if ((urls[1].find("contentType: text/html")>0) or (urls[1].find("contentType: application/xhtml")>0)):
                if "Content-Length=0" not in urls[1]:
                    startIndex = urls[1].find("Content:")
                    startIndex+=len("Content:")
                    stringHtml = urls[1][startIndex:]
                    try:
                        parsed = parser.from_buffer(stringHtml)
                    except:
                        print "Unable to extend connection to tika server integrated with geo-topic parser"
                        exit()
                    solrDict=parsed["metadata"]
                    stringtext=parsed["content"]
                    # call to tika geo-topic parser
                    status, response = callServer('put', ServerEndpoint, '/rmeta', stringtext,
                    {'Content-Type' : 'application/geotopic'}, False)
                    geoMetaData=_parse((status,response))
                    docMetaData=solrDict.copy()
                    gtags={}
                    if 'metadata' in geoMetaData:
                        geoMetaData=geoMetaData["metadata"]
                        gtags=getGeoTags(geoMetaData)
                    index=formatIndex(docMetaData)
                    index.update(gtags)
                    index['id']=url_id
                    if stringtext !=None:
                        html_content_text=' '.join(stringtext.split())
                        if len(html_content_text) > 10:
                            index['content']=html_content_text
                    result=()
                    # parseDateText python module for extracting date from text extracted from html data
                    try:
                        result = p.parseDateText(stringtext)
                    except:
                        pass
                    if len(result) > 0:
                        timehh='0'+str(result[3]) if len(str(result[3]))==1 else str(result[3])
                        timemm='0'+str(result[4]) if len(str(result[4]))==1 else str(result[4])
                        timess='0'+str(result[5]) if len(str(result[5]))==1 else str(result[5])
                        dateStr = str(result[0])+'-'+str(result[1])+'-'+str(result[2])+'T'+timehh+':'+timemm+':'+timess+'Z'
                        index['Ad_Creation_Date']=dateStr

                    # call to ner tagger for extracting entities from text
                    try:
                        nerTagged=st.tag(stringtext.split())
                        ad_posted_by=[]
                        ad_locations=[]
                        for tupleName in nerTagged:
                            if tupleName[1] == 'PERSON':
                                ad_posted_by.append(tupleName[0])
                            if tupleName[1] == 'LOCATION':
                                ad_locations.append(tupleName[0])
                        if len(ad_posted_by)>0:
                            ad_posted_by=list(set(ad_posted_by))
                            index["Ad_Posted_By"]=ad_posted_by
                        if len(ad_locations)>0:
                            ad_locations=list(set(ad_locations))
                            index["Ad_Locations"]=ad_locations
                        # call to ctakes parser
                        ctakesData=parser.from_buffer(stringtext,'http://localhost:9090',False)
                        if "metadata" in ctakesData:
                            ctakesMeta=process_ctakes(ctakesData["metadata"])
                            if ctakesMeta.__len__()>0:
                                index.update(ctakesMeta)
                    except:
                        pass
                    # adding the prepared document to the index
                    s.add(index)
        x+=y
        s.commit()
        if len(listCrawl)!=y:
            break
s.commit()