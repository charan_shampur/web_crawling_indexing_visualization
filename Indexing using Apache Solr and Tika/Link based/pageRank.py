import sys, json
import dumpJSONforD3

dampingFactor = 0.85

def pageRank(webGraph):
    for url, value in webGraph.items():
        webGraph[url]['pageRank'] = 1.0

    dumpJSONforD3.generateD3JSON(webGraph)

    if not webGraph:
        return {}
    
    iterations = 20
    baseURL = webGraph.keys()[0]

    for i in range(0, iterations):
        # print "Starting iteration " + str(i) + "...."
        # print "pageRank of URL:" + baseURL + ": " + str(webGraph[baseURL]['pageRank']) + "\n"

        for url in sorted(webGraph, key = webGraph.get):
            sumOfProb = 0.0
            for link in webGraph[url]['links']:
                outLink = float(len(webGraph[link]['links']))
                pr = webGraph[link]['pageRank']
                sumOfProb += (pr/outLink)

            webGraph[url]['pageRank'] = (1 - dampingFactor) + (dampingFactor * sumOfProb)

    result = {}
    for url, value in webGraph.items():
        result[url] = webGraph[url]['pageRank']

    return result

