import json, sys
# import networkx as nx
# import matplotlib.pyplot as plt
import pageRank
import pprint


def addLink(fromURL, toURL):
    if fromURL not in webGraph.keys():
        webGraph[fromURL] = {}
        webGraph[fromURL]['links'] = []
    webGraph[fromURL]['links'].append(toURL)

    if toURL not in webGraph.keys():
        webGraph[toURL] = {}
        webGraph[toURL]['links'] = []
    webGraph[toURL]['links'].append(fromURL)

def month(date):
    m = date
    if len(date) > 6:
        m = date[0:4] + date[5:7]

    return m

global webGraph
webGraph = {}

with open(sys.argv[1]) as jsonFile:    
    rawJSON = json.load(jsonFile)

#rawJSON = rawJSON['response']['docs']

for i, info in enumerate(rawJSON):

    for compare in rawJSON[i+1:len(rawJSON)]:
        try:
            # All images/pages from texas from August 2014 
            if info['Geographic_NAME'] == compare['Geographic_NAME'] and \
                            month(info['Ad_Creation_Date']) == month(compare['Ad_Creation_Date']):
                addLink(info['id'], compare['id'])

        except:
            pass

        # All images/pages where the ads are posted by Mike Ross
        try:
            if info['Ad_Posted_By'] == compare['Ad_Posted_By']:
                addLink(info['id'], compare['id'])
        except:
            pass

        # All images/pages related to Magnum Rifle (Gun Type)
        # All images/pages related to Bushmaster AR (Gun Model)
        try:
            if sorted(info['Gun_Types_Sold']) == sorted(compare["Gun_Types_Sold"]) or \
                            sorted(info['Gun_Models_Sold']) == sorted(info['Gun_Models_Sold']):
                addLink(info['id'], compare['id'])
        except:
            pass


result = pageRank.pageRank(webGraph)

for i, info in enumerate(rawJSON):
    try:
        rawJSON[i]['pageRank'] = result[info['id']]
    except:
        rawJSON[i]['pageRank'] = 0.15
try:
    for info in rawJSON:
        info.pop("_version_", None)
except:
    pass

with open('pageRanked.json', 'w') as outfile:
    outfile.write(json.dumps(rawJSON, indent=4))

print (json.dumps(rawJSON))

# Uncomment below code to visualize the webGraph created
# labels = {}
# for i, n in enumerate(G.nodes()):
#     labels[n] = "url" + str(i)

# pr = nx.pagerank(G)

# for x in pr:
#     print x + ' ' + str(pr[x] * 1000)

# nx.draw(G, with_labels = True, pos=nx.random_layout(G), labels=labels, font_size=10)
# plt.show() # display

