import json

def generateD3JSON(webGraph):
    d3JSON = {}
    d3JSON['nodes'] = []
    d3JSON['links'] = []

    urlList = webGraph.keys()
    urlList = list(set(urlList))

    for i, url in enumerate(urlList):
        node = {}
        node['name'] = "URL" + str(i)
        node['group'] = i
        d3JSON['nodes'].append(node)

        for links in webGraph[url]['links']:
            link = {}
            link['source'] = urlList.index(url)
            link['target'] = urlList.index(links)
            link['value'] = 1
            d3JSON['links'].append(link)

    with open('d3pagerank.json', 'w') as outfile:
        outfile.write(json.dumps(d3JSON, indent=4))