__author__ = 'natraj'
import cherrypy
import solr
import datetime
import json
import pprint
import os
import webbrowser

class HelloWorld(object):
    @cherrypy.expose
    def index(self):
        return """<html>
          <head></head>
          <body>
            <center>Enter value to search in solr</center>
            <br>
            <form method="get" style="text-align: center" action="generate">
              <input type="text" value="" name="length" />
              <button type="submit">Give it now!</button>
            </form>
          </body>
        </html>"""

    @cherrypy.expose
    def generate(self, length):
        print length
        '''
        solr_interface = sunburnt.SolrInterface("http://localhost:8983/solr/")
        response1 = solr_interface.query(str(length)).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).paginate(start=10, rows=10000).execute()
        '''
        s = solr.SolrConnection("http://localhost:8983/solr")
        response=s.query(str(length),['Ad_Locations','Ad_Posted_By','Geographic_LATITUDE','Geographic_LONGITUDE','Geographic_NAME','Gun_Models_Sold','Gun_Types_Sold','Optional_LATITUDE*','Optional_LONGITUDE*','Optional_NAME*','content_type','description','id','keywords','title','pageRank','CTAKES_Measurement_Annotation','CTAKES_Range_Annotation','CTAKES_Date_Annotation'],rows=10)
        wp = open('file.txt','w')
        wp.write('[\n')
        resval = len(response.results)
        i=0
        for hit in response.results:
            i+=1
            str1 = str(hit)
            str2=str1.replace("u'","'")
            str2=str2.replace("'","\"")
            wp.write(str2)
            if i<resval:
                wp.write(',\n')
        wp.write('\n]')
        wp.close()
        os.system("python generateWeightedGraph.py file.txt")
        fire = webbrowser.get('firefox')
        fire.open('d3Visualization.html')

if __name__ == '__main__':
   cherrypy.quickstart(HelloWorld())
