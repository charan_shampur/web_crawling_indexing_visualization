import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import com.google.gson.Gson;

public class NutchPageRank {
	
	public HttpSolrServer solr;
	public NutchPageRank()
	{
		solr = new HttpSolrServer("http://localhost:8983/solr");
		
	}
	public SolrDocumentList getQueryResult()
	{
		SolrQuery query = new SolrQuery();
	    query.setQuery("*:*");
	    query.setRows(3000);
	    query.setStart(0);
	    query.setFields("Ad_Creation_Date", "Ad_Locations",
	    		"Ad_Posted_By", "Geographic_LATITUDE",
	    		"Geographic_LONGITUDE", "Geographic_NAME",
	    		"Gun_Models_Sold", "Gun_Types_Sold",
	    		"Optional_LATITUDE*", "Optional_LONGITUDE*",
	    		"Optional_NAME*", "content", "content_type",
	    		"description", "id", "keywords", "title",
	    		"CTAKES_Range_Annotation", "CTAKES_Date_Annotation", 
	    		"CTAKES_Measurement_Annotation");    
	    query.setParam("wt", "json");
	    QueryResponse response = null;
		try {
			response = solr.query(query);
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	    SolrDocumentList results = response.getResults();
		return results;	
	}
	
	public void read_json_and_annotate(String strobj)
	{
		int commitCount = 0;
		org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
		try {
			JSONArray jsonArray = (JSONArray) parser.parse(strobj);
			for(int i=0;i<jsonArray.size();i++)
			{
				commitCount++;
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				annotatePagerank(jsonObject);
				if(commitCount>100)
				{
					solr.commit();
					commitCount=0;
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}	
	}
	
	public void annotatePagerank(JSONObject jsonObject)
	{
		System.out.println("Annotating page rank to : "+jsonObject.get("id"));
		SolrInputDocument document = new SolrInputDocument();
		document.addField("id", jsonObject.get("id"));
		Map<String,Double> pageMap = new HashMap<>(1);
		pageMap.put("add", (Double) jsonObject.get("pageRank"));
		document.addField("pageRank", pageMap);
		try {
			solr.add(document);
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeSolrDocument(SolrDocumentList doclist) throws IOException
	{
		String homeDirString = System.getProperty("user.home");
		new File(homeDirString+"/NutchPageRank").mkdir();
		FileWriter file = new FileWriter(homeDirString+"/NutchPageRank/input.json");
		file.write("["+'\n');
		for(int i=0;i<doclist.size();i++)
		{
			//System.out.println(doclist.get(i));
			Gson gson = new Gson();
			String json = gson.toJson(doclist.get(i)).toString();
			//System.out.println(json);
			if(i==doclist.size()-1){
				file.write(json.toString()+'\n');
				file.flush();
			}
			else{
				file.write(json.toString()+",\n");
				file.flush();
			}
		}
		file.write(']');
		file.flush();
		file.close();
		System.out.println("Successfully Copied JSON Object to File in location : "+homeDirString+"/NutchPageRank/input.json");
	}
	
	public static void main(String[] args) throws IOException
	{
		System.out.println("Running my custom class");
		System.out.println("Initializing class - checking connection to solr server");
		NutchPageRank nutchObj=new NutchPageRank();
		System.out.println("Downloading indexed documents from SOLR...");
		SolrDocumentList docList1=nutchObj.getQueryResult();
		nutchObj.writeSolrDocument(docList1);
		try {
			String homeDirString = System.getProperty("user.home");
			String pythonArgString = homeDirString + "/NutchPageRank/input.json";
			System.out.println("Executing python link analysis program generateWeightedGraph.py");
			Process p = Runtime.getRuntime().exec("python generateWeightedGraph.py "+ pythonArgString);
			BufferedReader stdInput = new BufferedReader(new
	                 InputStreamReader(p.getInputStream()));
			String jsonString = stdInput.readLine();
			nutchObj.read_json_and_annotate(jsonString);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
}
