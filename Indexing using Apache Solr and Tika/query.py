import sunburnt
import datetime
import operator
import sys
from collections import defaultdict

def query1():
    print ""
    print "Query 1:Time based and location based gun AD trends-Content based algorithm"
    print ""
    yr1=yr2=2007
    mont1=1
    maxstr=''
    maxval=0
    while 1:
        mont2 = mont1+6
        if mont2>12:
            mont2=1
        if yr2==2016 and mont2>1:
            break
        response = solr_interface.query(Ad_Creation_Date__range=(datetime.datetime(yr1, mont1, 1), datetime.datetime(yr2, mont2, 1))).execute()
        if maxval < response.result.numFound:
            maxval = response.result.numFound
            maxstr=str(yr1)+'_'+str(mont1)+'_'+str(yr2)+'_'+str(mont2)
        yr1=yr2
        if mont2 != 1:
            yr2+=1
        mont1=mont2
    list1 = maxstr.split('_')
    response = solr_interface.query(Ad_Creation_Date__range=(datetime.datetime(int(list1[0]), int(list1[1]), 1), datetime.datetime(int(list1[2]), int(list1[3]), 1))).paginate(start=10, rows=10).execute()
    print "The time period from " + str(list1[0]) +"-"+str(list1[1]) +" to "+str(list1[2]) +"-"+str(list1[3]) + " has highest number of Ad posts:"
    for result in response:
        print result["id"]
    response1 = solr_interface.query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).boost_relevancy(3).paginate(start=10, rows=10000).execute()
    print ""
    print "The Ad's posted between 10PM to 12PM:"
    for r2 in response1:
        if int(r2["Ad_Creation_Date"].ctime()[11:13]) >=22 and int(r2["Ad_Creation_Date"].ctime()[11:12]) < 24:
            print str(r2["Ad_Creation_Date"].ctime()) + " : "+ r2["id"]

    response1 = solr_interface.query(Ad_Locations="*").boost_relevancy(3).field_limit(["id", "Ad_Locations"]).paginate(start=10, rows=10).execute()
    d = defaultdict(list)
    for result in response1:
        for clist in result["Ad_Locations"]:
            d[clist].append(result["id"])
    maxv=0
    cityname=''
    for keys in d.keys():
        if maxv<len(d[keys]):
            maxv=len(d[keys])
            cityname=keys
    print ""
    print "Place from where highest number of Ad's where posted: " + cityname
    for r1 in d[cityname]:
        print r1

def query11():
    print ""
    print "Query 1:Time based and location based gun AD trends-Link based algorithm"
    print ""
    yr1=yr2=2007
    mont1=1
    maxstr=''
    maxval=0
    while 1:
        mont2 = mont1+6
        if mont2>12:
            mont2=1
        if yr2==2016 and mont2>1:
            break
        response = solr_interface.query(Ad_Creation_Date__range=(datetime.datetime(yr1, mont1, 1), datetime.datetime(yr2, mont2, 1))).execute()
        if maxval < response.result.numFound:
            maxval = response.result.numFound
            maxstr=str(yr1)+'_'+str(mont1)+'_'+str(yr2)+'_'+str(mont2)
        yr1=yr2
        if mont2 != 1:
            yr2+=1
        mont1=mont2
    list1 = maxstr.split('_')
    response = solr_interface.query(Ad_Creation_Date__range=(datetime.datetime(int(list1[0]), int(list1[1]), 1), datetime.datetime(int(list1[2]), int(list1[3]), 1))).sort_by("-pageRank").paginate(start=10, rows=10).execute()
    print "The time period from " + str(list1[0]) +"-"+str(list1[1]) +" to "+str(list1[2]) +"-"+str(list1[3]) + " has highest number of Ad posts:"
    for result in response:
        print result["id"]
    response1 = solr_interface.query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).boost_relevancy(3).sort_by("-pageRank").paginate(start=10, rows=10000).execute()
    print ""
    print "The Ad's posted between 10PM to 12PM:"
    for r2 in response1:
        if int(r2["Ad_Creation_Date"].ctime()[11:13]) >=22 and int(r2["Ad_Creation_Date"].ctime()[11:12]) < 24:
            print str(r2["Ad_Creation_Date"].ctime()) + " : "+ r2["id"]

    response1 = solr_interface.query(Ad_Locations="*").boost_relevancy(3).field_limit(["id", "Ad_Locations"]).sort_by("-pageRank").paginate(start=10, rows=10).execute()
    d = defaultdict(list)
    for result in response1:
        for clist in result["Ad_Locations"]:
            d[clist].append(result["id"])
    maxv=0
    cityname=''
    for keys in d.keys():
        if maxv<len(d[keys]):
            maxv=len(d[keys])
            cityname=keys
    print ""
    print "Place from where highest number of Ad's where posted: " + cityname
    for r1 in d[cityname]:
        print r1


def query2():
    cdict={}
    print ""
    print "Query 2:Fire arms sold in same region and time-Content based algorithm:"
    print ""
    keylist = ["Long gun","Blunderbuss","Musket","Wall gun","Grenade","Rifle","Lever-action rifle","Bolt-action","Assault rifle","Battle rifle","Carbine","Sniper rifle","Scattergun","Combat Tactical Handgun","Combat pistols","combat handguns","Semi-automatic shotgun","Automatic shotgun","Gatling gun","Submachine gun","Machine gun","Infantry Rifle","Handguns","Revolver","Field gun","Howitzer","Shotgun","Varmint rifle"]
    for val in keylist:
        print "Results for: " + val
        print ""
        response2 = solr_interface.query(solr_interface.Q(content=val)**3 | solr_interface.Q(id=val) | solr_interface.Q(title=val) | solr_interface.Q(description=val)).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).field_limit(["id","Ad_Creation_Date", "Ad_Locations"]).paginate(start=10, rows=10).execute()
        d = defaultdict(list)
        for result in response2:
            if len(result) == 3:
                cdict[result["Ad_Creation_Date"]] = result["id"]
                for cval in result["Ad_Locations"]:
                    d[cval].append(cdict)
        for keys in d.keys():
            tkeys = d[keys][0].keys()
            fdict={}
            for ky in tkeys:
                start_date = ky+datetime.timedelta(hours=1)
                end_date = ky+datetime.timedelta(hours=-1)
                count1=0
                strf = str(ky) + "#"
                for ky1 in tkeys:
                    if end_date <= ky1 <= start_date:
                        count1+=1
                        strf += "_"+str(ky1)
                fdict[strf]=count1
            fkey = max(fdict.iteritems(), key=operator.itemgetter(1))[0]
            timek=str(fkey).split('#')[0]
            ulist = str(fkey).split('#')[1].split('_')
            print "City:" + str(keys) + " Time:" + str(timek)
            i=0
            for urll in ulist:
                if i >5:
                    break
                i+=1
                if urll != "":
                    print str(d[keys][0][datetime.datetime.strptime(urll, "%Y-%m-%d %H:%M:%S")])
            print ""

def query22():
    cdict={}
    print ""
    print "Query 2:Fire arms sold in same region and time-Link based algorithm:"
    print ""
    keylist = ["Long gun","Blunderbuss","Musket","Wall gun","Grenade","Rifle","Lever-action rifle","Bolt-action","Assault rifle","Battle rifle","Carbine","Sniper rifle","Scattergun","Combat Tactical Handgun","Combat pistols","combat handguns","Semi-automatic shotgun","Automatic shotgun","Gatling gun","Submachine gun","Machine gun","Infantry Rifle","Handguns","Revolver","Field gun","Howitzer","Shotgun","Varmint rifle"]
    for val in keylist:
        print "Results for: " + val
        print ""
        response2 = solr_interface.query(solr_interface.Q(content=val)**3 | solr_interface.Q(id=val) | solr_interface.Q(title=val) | solr_interface.Q(description=val)).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).field_limit(["id","Ad_Creation_Date", "Ad_Locations"]).sort_by("-pageRank").paginate(start=10, rows=10).execute()
        d = defaultdict(list)
        for result in response2:
            if len(result) == 3:
                cdict[result["Ad_Creation_Date"]] = result["id"]
                for cval in result["Ad_Locations"]:
                    d[cval].append(cdict)
        for keys in d.keys():
            tkeys = d[keys][0].keys()
            fdict={}
            for ky in tkeys:
                start_date = ky+datetime.timedelta(hours=1)
                end_date = ky+datetime.timedelta(hours=-1)
                count1=0
                strf = str(ky) + "#"
                for ky1 in tkeys:
                    if end_date <= ky1 <= start_date:
                        count1+=1
                        strf += "_"+str(ky1)
                fdict[strf]=count1
            fkey = max(fdict.iteritems(), key=operator.itemgetter(1))[0]
            timek=str(fkey).split('#')[0]
            ulist = str(fkey).split('#')[1].split('_')
            print "City:" + str(keys) + " Time:" + str(timek)
            i=0
            for urll in ulist:
                if i >5:
                    break
                i+=1
                if urll != "":
                    print str(d[keys][0][datetime.datetime.strptime(urll, "%Y-%m-%d %H:%M:%S")])
            print ""


def query3():
    list1 = ['Ruger', 'Springfield', 'Colt 45', 'Rifle', 'Caliber', 'Handgun', 'Auto Ordnance 1911', 'Baby Desert Eagle', 'Barrett 50', 'Barrett M107', 'Benelli M2', 'Benelli M4', 'Benelli Nova', 'Benelli R1', 'Beretta 70S', 'Beretta 92 FS', 'Beretta 92FS INOX', 'Beretta A303', 'Beretta CX4', 'Beretta Nano', 'Beretta Px4 Storm', 'Beretta Tomcat', 'Bersa Thunder 380', 'Blaser R8', 'Blaser R93', 'Bond Arms Snake Slayer', 'Browning A Bolt', 'Browning A5', 'Browning BAR', 'Browning Buckmark', 'Browning Maxus', 'Browning X Bolt', 'Bushmaster AR', 'Bushmaster M4', 'Colt 1903', 'Colt 1911', 'Colt 38 Super', 'Colt 380', 'Colt 45', 'Colt AR 15', 'Colt Cobra', 'Colt Defender', 'Colt Dragoon', 'Colt M4', 'Colt Mustang', 'Colt Peacemaker', 'Colt Python', 'Coonan 357', 'CVA Buckhorn', 'CZ 52', 'CZ 75', 'CZ 75 B', 'Desert Eagle', 'Diamondback 380', 'Diamondback DB9', 'DPMS AR 15', 'EAA Witness', 'Ed Brown 1911', 'Eotech 512', 'FN FAL', 'FN Five Seven', 'FN FS2000', 'FN P90', 'FN PS90', 'FN SCAR', 'Glenfield Model 75', 'Glock 17', 'Glock 18', 'Glock 19', 'Glock 20', 'Glock 21', 'Glock 22', 'Glock 23', 'Glock 26', 'Glock 27', 'Glock 30', 'Glock 34', 'Glock 36', 'Glock 40', 'Glock 45 ACP', 'GSG 5', 'Hi Point 45', 'Hi-Point 9MM Carbine', 'HK MP5K', 'HK USP', 'HK416', 'HK45', 'Jericho 941', 'Jimenez Arms 380', 'K98 Mauser', 'Kahr CM9', 'Kahr P9', 'Kahr PM40', 'Kahr PM9', 'Kel Tec 380', 'Kel Tec P11', 'Kel Tec P32', 'Kel Tec P-3AT', 'Kel Tec Sub 2000', 'KelTec PF9', 'Kimber 1911', 'Kimber 45', 'Marlin Model 80', 'Maverick 88', 'Micro Uzi', 'Mini Uzi', 'Mosin Nagant M38', 'Mosin Nagant M44', 'Mossberg 464', 'Mossberg 500', 'Mossberg 51663', 'Mossberg 590', 'Mossberg 702 Plinkster', 'Mossberg 930', 'Mossberg 935', 'Mossberg 935', 'Mossberg Mariner', 'Mossberg Persuader', 'Norinco SKS', 'P Beretta', 'Pocket Pistols', 'Px4 Storm', 'Remington 1100', 'Remington 1187', 'Remington 1911', 'Remington 22 Rifle', 'Remington 223', 'Remington 597', 'Remington 700', 'Remington 700 ADL', 'Remington 700 Police', 'Remington 700 Sendero', 'Remington 700 SPS Varmint', 'Remington 700 Tactical', 'Remington 700 VTR', 'Remington 700 XCR', 'Remington 710', 'Remington 750', 'Remington 770', 'Remington 870', 'Remington 870 MCS', 'Remington Model 10', 'Remington Model 11', 'Remington Model 11-48', 'Remington Model 7', 'Remington Versa Max', 'Rossi Circuit Judge', 'Ruger 10/22', 'Ruger 10-22 Magnum', 'Ruger 22', 'Ruger 22 45', 'Ruger 22 Pistol', 'Ruger 22 Rifle', 'Ruger 357', 'Ruger 44 Carbine', 'Ruger 77 22', 'Ruger 9mm', 'Ruger Alaskan', 'Ruger Bearcat', 'Ruger Blackhawk', 'Ruger Charger', 'Ruger GP100', 'Ruger LCP', 'Ruger LCR', 'Ruger M77', 'Ruger Mark 2', 'Ruger Mark II', 'Ruger Mark III', 'Ruger Mini 14', 'Ruger Mini 30', 'Ruger Old Army Revolvers', 'Ruger P345', 'Ruger P89', 'Ruger P94', 'Ruger P95', 'Ruger Security Six', 'Ruger Single Six', 'Ruger SP101', 'Ruger Super Blackhawk', 'Ruger Super Redhawk', 'Ruger Vaquero', 'RWS Diana', 'Saiga 12', 'Saiga 223', 'Saiga 308', 'Sako 75', 'Sako 85', 'Sako TRG 42', 'Savage 10FP', 'Savage 111', 'Savage 24C', 'Savage 93R17', 'Savage Axis', 'Savage Mark 2', 'Savage Mark II', 'Savage Model 64', 'SIG P220', 'SIG P239', 'Sig Sauer Mosquito', 'Sig Sauer P225', 'Sig Sauer P226', 'Sig Sauer P228', 'Sig Sauer P229', 'Sig Sauer P250', 'Slide Fire AR 15', 'Smith and Wesson 357 Magnum', 'Smith and Wesson 40', 'Smith and Wesson 41', 'Smith and Wesson 500', 'Smith and Wesson Bodyguard', 'Smith And Wesson Governor', 'Smith and Wesson M&P', 'Smith and Wesson Model 65', 'Smith and Wesson SD40', 'Smith and Wesson Sigma', 'Springfield 1911', 'Springfield XD', 'Springfield XD 40', 'Springfield XDM', 'Steyr AUG', 'STG 44', 'Stoeger Condor', 'SW9VE', 'Taurus 24 7', 'Taurus 357', 'Taurus 357 Magnum', 'Taurus 85', 'TEC 9', 'The Judge Gun', 'Tikka T3 Lite', 'USP 45', 'Uzi 9mm', 'Varmint Rifle', 'Walther P22', 'Walther P38', 'Walther P99', 'Walther PPK', 'Weatherby Vanguard', 'Winchester 1873', 'Winchester 1892', 'Winchester 22 Rifles', 'Winchester 73', 'Winchester Model 12', 'Winchester Model 290', 'Winchester Model 70', 'Winchester Model 94']
    print ""
    print "Query 3: Tracking lost shipments - Content based algorithm"
    print ""
    for lval in list1:
        response3 = solr_interface.query(solr_interface.Q(content=lval)**3 | solr_interface.Q(title=lval) | solr_interface.Q(description=lval)**3).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).field_limit(["id","Ad_Creation_Date"]).paginate(start=10, rows=10).execute()
        if response3.result.numFound >0:
            print "Model: " + lval
            for result in response3:
                print result["Ad_Creation_Date"].ctime() + ": " + result['id']
            print ''

def query33():
    list1 = ['Ruger', 'Springfield', 'Colt 45', 'Rifle', 'Caliber', 'Handgun', 'Auto Ordnance 1911', 'Baby Desert Eagle', 'Barrett 50', 'Barrett M107', 'Benelli M2', 'Benelli M4', 'Benelli Nova', 'Benelli R1', 'Beretta 70S', 'Beretta 92 FS', 'Beretta 92FS INOX', 'Beretta A303', 'Beretta CX4', 'Beretta Nano', 'Beretta Px4 Storm', 'Beretta Tomcat', 'Bersa Thunder 380', 'Blaser R8', 'Blaser R93', 'Bond Arms Snake Slayer', 'Browning A Bolt', 'Browning A5', 'Browning BAR', 'Browning Buckmark', 'Browning Maxus', 'Browning X Bolt', 'Bushmaster AR', 'Bushmaster M4', 'Colt 1903', 'Colt 1911', 'Colt 38 Super', 'Colt 380', 'Colt 45', 'Colt AR 15', 'Colt Cobra', 'Colt Defender', 'Colt Dragoon', 'Colt M4', 'Colt Mustang', 'Colt Peacemaker', 'Colt Python', 'Coonan 357', 'CVA Buckhorn', 'CZ 52', 'CZ 75', 'CZ 75 B', 'Desert Eagle', 'Diamondback 380', 'Diamondback DB9', 'DPMS AR 15', 'EAA Witness', 'Ed Brown 1911', 'Eotech 512', 'FN FAL', 'FN Five Seven', 'FN FS2000', 'FN P90', 'FN PS90', 'FN SCAR', 'Glenfield Model 75', 'Glock 17', 'Glock 18', 'Glock 19', 'Glock 20', 'Glock 21', 'Glock 22', 'Glock 23', 'Glock 26', 'Glock 27', 'Glock 30', 'Glock 34', 'Glock 36', 'Glock 40', 'Glock 45 ACP', 'GSG 5', 'Hi Point 45', 'Hi-Point 9MM Carbine', 'HK MP5K', 'HK USP', 'HK416', 'HK45', 'Jericho 941', 'Jimenez Arms 380', 'K98 Mauser', 'Kahr CM9', 'Kahr P9', 'Kahr PM40', 'Kahr PM9', 'Kel Tec 380', 'Kel Tec P11', 'Kel Tec P32', 'Kel Tec P-3AT', 'Kel Tec Sub 2000', 'KelTec PF9', 'Kimber 1911', 'Kimber 45', 'Marlin Model 80', 'Maverick 88', 'Micro Uzi', 'Mini Uzi', 'Mosin Nagant M38', 'Mosin Nagant M44', 'Mossberg 464', 'Mossberg 500', 'Mossberg 51663', 'Mossberg 590', 'Mossberg 702 Plinkster', 'Mossberg 930', 'Mossberg 935', 'Mossberg 935', 'Mossberg Mariner', 'Mossberg Persuader', 'Norinco SKS', 'P Beretta', 'Pocket Pistols', 'Px4 Storm', 'Remington 1100', 'Remington 1187', 'Remington 1911', 'Remington 22 Rifle', 'Remington 223', 'Remington 597', 'Remington 700', 'Remington 700 ADL', 'Remington 700 Police', 'Remington 700 Sendero', 'Remington 700 SPS Varmint', 'Remington 700 Tactical', 'Remington 700 VTR', 'Remington 700 XCR', 'Remington 710', 'Remington 750', 'Remington 770', 'Remington 870', 'Remington 870 MCS', 'Remington Model 10', 'Remington Model 11', 'Remington Model 11-48', 'Remington Model 7', 'Remington Versa Max', 'Rossi Circuit Judge', 'Ruger 10/22', 'Ruger 10-22 Magnum', 'Ruger 22', 'Ruger 22 45', 'Ruger 22 Pistol', 'Ruger 22 Rifle', 'Ruger 357', 'Ruger 44 Carbine', 'Ruger 77 22', 'Ruger 9mm', 'Ruger Alaskan', 'Ruger Bearcat', 'Ruger Blackhawk', 'Ruger Charger', 'Ruger GP100', 'Ruger LCP', 'Ruger LCR', 'Ruger M77', 'Ruger Mark 2', 'Ruger Mark II', 'Ruger Mark III', 'Ruger Mini 14', 'Ruger Mini 30', 'Ruger Old Army Revolvers', 'Ruger P345', 'Ruger P89', 'Ruger P94', 'Ruger P95', 'Ruger Security Six', 'Ruger Single Six', 'Ruger SP101', 'Ruger Super Blackhawk', 'Ruger Super Redhawk', 'Ruger Vaquero', 'RWS Diana', 'Saiga 12', 'Saiga 223', 'Saiga 308', 'Sako 75', 'Sako 85', 'Sako TRG 42', 'Savage 10FP', 'Savage 111', 'Savage 24C', 'Savage 93R17', 'Savage Axis', 'Savage Mark 2', 'Savage Mark II', 'Savage Model 64', 'SIG P220', 'SIG P239', 'Sig Sauer Mosquito', 'Sig Sauer P225', 'Sig Sauer P226', 'Sig Sauer P228', 'Sig Sauer P229', 'Sig Sauer P250', 'Slide Fire AR 15', 'Smith and Wesson 357 Magnum', 'Smith and Wesson 40', 'Smith and Wesson 41', 'Smith and Wesson 500', 'Smith and Wesson Bodyguard', 'Smith And Wesson Governor', 'Smith and Wesson M&P', 'Smith and Wesson Model 65', 'Smith and Wesson SD40', 'Smith and Wesson Sigma', 'Springfield 1911', 'Springfield XD', 'Springfield XD 40', 'Springfield XDM', 'Steyr AUG', 'STG 44', 'Stoeger Condor', 'SW9VE', 'Taurus 24 7', 'Taurus 357', 'Taurus 357 Magnum', 'Taurus 85', 'TEC 9', 'The Judge Gun', 'Tikka T3 Lite', 'USP 45', 'Uzi 9mm', 'Varmint Rifle', 'Walther P22', 'Walther P38', 'Walther P99', 'Walther PPK', 'Weatherby Vanguard', 'Winchester 1873', 'Winchester 1892', 'Winchester 22 Rifles', 'Winchester 73', 'Winchester Model 12', 'Winchester Model 290', 'Winchester Model 70', 'Winchester Model 94']
    print ""
    print "Query 3: Tracking lost shipments - Link based algorithm"
    print ""
    for lval in list1:
        response3 = solr_interface.query(solr_interface.Q(content=lval)**3 | solr_interface.Q(title=lval) | solr_interface.Q(description=lval)**3).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).field_limit(["id","Ad_Creation_Date"]).sort_by("-pageRank").paginate(start=10, rows=10).execute()
        if response3.result.numFound >0:
            print "Model: " + lval
            for result in response3:
                print result["Ad_Creation_Date"].ctime() + ": " + result['id']
            print ''


def query4():
    print ""
    print "Query 4: Identifying Ads posted by underage users-Content based algorithm"
    print ""
    response4 = solr_interface.query(solr_interface.Q(content="kid")**3 | solr_interface.Q(title="kid")**4 | solr_interface.Q(description="kid")**2).field_limit(["id","Ad_Posted_By","content"]).paginate(start=10, rows=10000).execute()
    d = defaultdict(list)
    for result in response4:
        if len(result) == 3:
            for users in result["Ad_Posted_By"]:
                if 'retired' not in result["content"] or 'resposible' not in result["content"]:
                    d[users].append(result["id"])
    for eachkey in d.keys():
        print eachkey
        for eachurl in d[eachkey]:
            print str(eachurl).encode('ascii', 'ignore')
        print ""

def query44():
    print ""
    print "Query 4: Identifying Ads posted by underage users-Link based algorithm"
    print ""
    response4 = solr_interface.query(solr_interface.Q(content="kid")**3 | solr_interface.Q(title="kid")**4 | solr_interface.Q(description="kid")**2).field_limit(["id","Ad_Posted_By","content","pageRank"]).sort_by("-pageRank").paginate(start=10, rows=10000).execute()
    d = defaultdict(list)
    for result in response4:
        if len(result) == 4:
            for users in result["Ad_Posted_By"]:
                if 'retired' not in result["content"] or 'resposible' not in result["content"]:
                    d[users].append(result["id"])
    for eachkey in d.keys():
        print eachkey
        for eachurl in d[eachkey]:
            print str(eachurl).encode('ascii', 'ignore')
        print ""


def query5():
    print ""
    print "Query 5: Ad's related to explosives or WMD-Content based algorithm"
    print ""
    Elist=["Dynamite","granade","Explosive","Tannerite","c-4","IED","TNT"]
    d = defaultdict(list)
    for eachl in Elist:
        listf=[]
        response5 = solr_interface.query(solr_interface.Q(content=eachl)**3 | solr_interface.Q(title=eachl) | solr_interface.Q(description=eachl)).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).boost_relevancy(4).field_limit(["id","Ad_Posted_By"]).paginate(start=10, rows=10).execute()
        for result in response5:
            if result["id"] not in listf:
                listf.append(result["id"])
        d[eachl] = listf
    for val in d.keys():
        print str(val) + ":"
        for val2 in d[val]:
            print val2
        print ""

def query55():
    print ""
    print "Query 5: Ad's related to explosives or WMD-Link based algorithm"
    print ""
    Elist=["Dynamite","granade","Explosive","Tannerite","c-4","IED","TNT"]
    d = defaultdict(list)
    for eachl in Elist:
        listf=[]
        response5 = solr_interface.query(solr_interface.Q(content=eachl)**3 | solr_interface.Q(title=eachl) | solr_interface.Q(description=eachl)).query(Ad_Creation_Date__lt=datetime.datetime(2016, 1, 1)).boost_relevancy(4).field_limit(["id","Ad_Posted_By"]).sort_by("-pageRank").paginate(start=10, rows=10).execute()
        for result in response5:
            if result["id"] not in listf:
                listf.append(result["id"])
        d[eachl] = listf
    for val in d.keys():
        print str(val) + ":"
        for val2 in d[val]:
            print val2
        print ""



solr_interface = sunburnt.SolrInterface("http://localhost:8983/solr/")
option1 = raw_input("Please enter 1 for content based and 2 for linked based algorithm or any key to exit:")
while option1=='1' or option1=='2':
    if option1=='1':
        print ""
        opt2 = raw_input("Please choose from 1-5 to run the respective query for content based:")
        if opt2 == '1':
            query1()
        elif opt2 == '2':
            query2()
        elif opt2 =='3':
            query3()
        elif opt2 =='4':
            query4()
        elif opt2 =='5':
            query5()
        else:
            print "Not a valid option"
    if option1 =='2':
        print ""
        opt2 = raw_input("Please choose from 1-5 to run the respective query for link based algorithm:")
        if opt2 == '1':
            query11()
        elif opt2 == '2':
            query22()
        elif opt2 =='3':
            query33()
        elif opt2 =='4':
            query44()
        elif opt2 =='5':
            query55()
        else:
            print "Not a valid option"
    print ""
    option1 = raw_input("Please enter 1 for content based and 2 for linked based algorithm or any key to exit:")
