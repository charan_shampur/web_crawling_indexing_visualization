import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class FieldAnnotator {
	
	public HttpSolrServer solr;
	
	public FieldAnnotator() {
		// TODO Auto-generated constructor stub
		solr = new HttpSolrServer("http://localhost:8983/solr");
	}
	
	//Function reads the JSON file obtained from the link analysis algorithm which has page ranks associated with each url 
	public void readJson(File file){
		int commitCount=0;
		JSONParser parser = new JSONParser();
		JSONArray jsonArray;
		try {
			jsonArray = (JSONArray) parser.parse(new FileReader(file));
			for(int i=0;i<jsonArray.size();i++)
			{
				try{
					JSONObject jsonDoc = (JSONObject) jsonArray.get(i);
					System.out.println(jsonDoc.get("id"));
					annotate(jsonDoc);
					commitCount++;
					if(commitCount==300){
						solr.commit();
						commitCount=0;
					}
				}
				catch(Exception e){
					System.out.println("Error while reading json object"+e.getMessage());
				}
			}
		}catch(Exception e){
			System.out.println("Error while parsing json object");
		}
	}
	
	// This function annotates the pageRank for each url in the solr index 
	public void annotate(JSONObject jsonObject)
	{
		SolrInputDocument doc=new SolrInputDocument();
		doc.addField("id", jsonObject.get("id").toString());
		Map<String,Double> newFieldMap = new HashMap<>(1);
		newFieldMap.put("add", (Double)jsonObject.get("pageRank"));
		doc.addField("pageRank",newFieldMap);
		try {
			solr.add(doc);
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			System.out.println("problem while adding the document-"+e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//main function
	public static void main(String[] args)
	{
		if(args.length<1){
			System.out.println("exeution : java FieldAnnotator.java <path to json file from link relevancy algorithm> ");
			System.exit(0);
		}			
		FieldAnnotator faAnnotator = new FieldAnnotator();
		File file = new File(args[0]);
		faAnnotator.readJson(file);
	}
}
