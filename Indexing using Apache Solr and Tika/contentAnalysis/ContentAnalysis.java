import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContentAnalysis {
	
	// hashmap to store the important keywords(gun types for eg rifle,shotgun etc)  and an Integer array to store [(weight of the keyword), (no of times appeared in the document)]
	public HashMap<String,Integer[]> gunTerms = new HashMap<String,Integer[]>();
	// Array to store the different gun models read from the file like (ak47,ar15,bushmaster,etc...)
	public ArrayList<String> gunModels = new ArrayList<String>();  
	public HttpSolrServer solr;
	//New field that will be indexed into solr
	public ArrayList<String> gunTypeField = new ArrayList<String>();
	//New field that will be indexed into solr
	public ArrayList<String> gunModelField = new ArrayList<String>();
	
	//The constructor reads the gunModel and the gunType text files and also establish a connection to solr Server
	ContentAnalysis(String gunTypePath, String gunModelPath)
	{
		String line;
		int weight=0;
		try{
		File gunTypeFile = new File(gunTypePath);
		if (gunTypeFile.exists() && gunTypeFile.length()!=0){
				BufferedReader in1 = new BufferedReader(new FileReader(gunTypeFile));
				while((line=in1.readLine())!=null){
					Integer[] arr = {0,weight};
					gunTerms.put(line, arr);
					weight++;
				}
			}
		}
		catch( Exception e){
			System.out.println("Error while reading the input file gunTypes.txt "+e.getMessage());
		}
		try{
		File gunModelFile = new File(gunModelPath);
		if(gunModelFile.exists() && gunModelFile.length()!=0){
				BufferedReader in2 = new BufferedReader(new FileReader(gunModelFile));
				while((line=in2.readLine())!=null)
					gunModels.add(line);
			}
		}
		catch(Exception e) {
			System.out.println("Error while reading the input file gunModels.txt"+e.getMessage());
		}
		solr = new HttpSolrServer("http://localhost:8983/solr");
	}
	
	//function to update the indexed documents with additional fields Gun_Types_Sold, Gun_Models_Sold.
	//This function also does flat boost on field and documents if and only if the content is relevant to our queries. 
	public void index(JSONObject jsonDoc) 
	{
		double boost=1.0f;
		try{
			SolrInputDocument doc=new SolrInputDocument();
			if(jsonDoc.containsKey("content"))
			{
				String data=jsonDoc.get("content").toString();
				boost=calculateTermFrequency(data);
				if(gunTypeField.size()>0)
				{
					Map<String,ArrayList<String>> gunTypeMap = new HashMap<>(1);
					gunTypeMap.put("add", gunTypeField);
					doc.addField("Gun_Types_Sold", gunTypeMap, (float)boost);
					doc.setDocumentBoost(10.0f);
				}
				if(gunModelField.size()>0)
				{
					Map<String,ArrayList<String>> gunModelMap = new HashMap<>(1);
					gunModelMap.put("add", gunModelField);
					doc.addField("Gun_Models_Sold", gunModelMap, (float)boost);
					doc.setDocumentBoost(10.0f);
				}
				doc.addField("id", jsonDoc.get("id").toString());
				doc.setField("content", jsonDoc.get("content"), (float)boost);
				UpdateResponse response=solr.add(doc);
			}
		}catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e){
			System.out.println("Error with solr server"+e.getMessage());
		}
	}
	
	//This function calculates the term frequency of the relevant keywords and obtains a boosting factor in the order of the term frequency
	public double calculateTermFrequency(String content)
	{
		gunTypeField.clear();
		gunModelField.clear();
		double termFreq=1.0f;
		int totalTerms=content.length();
		if(totalTerms>0)
		{
			for(Map.Entry<String, Integer[]> entry : gunTerms.entrySet())
			{
				int count = StringUtils.countMatches(content.toLowerCase(), entry.getKey().toLowerCase());
				if (count > 0)
					gunTypeField.add(entry.getKey().toString());
				Integer[] countArr = entry.getValue();
				countArr[0]=count;
				entry.setValue(countArr);
			}
			
			for(String gunModel : gunModels)
			{
				if (content.contains(gunModel))
					gunModelField.add(gunModel);
			}
			double maxCount=0;
			String maxTerm="";
			for(Map.Entry<String, Integer[]> entry : gunTerms.entrySet())
			{
				if(entry.getValue()[0]>maxCount)
				{
					maxCount=entry.getValue()[0];
					maxTerm=entry.getKey().toString();
				}
			}
			if (maxCount > 0)
			{
				int multFactor=gunTerms.get(maxTerm)[1];
				double prob = maxCount/50.0;
				if(prob > 1) prob=1;
				termFreq = (gunTerms.size()-multFactor) * prob * 10;
			}
			else
				termFreq=1.0f;
		}
		return termFreq;
	}
	
	//This function reads the json data which is downloaded from NER tagged solr indexes
	public void getJsonNerData(File name)
	{
		int commitCount=0;
		JSONParser parser = new JSONParser();
		JSONArray jsonArray;
		try {
			jsonArray = (JSONArray) parser.parse(new FileReader(name));
			for(int i=0;i<jsonArray.size();i++)
			{
				try{
					JSONObject jsonDoc = (JSONObject) jsonArray.get(i);
					System.out.println(jsonDoc.get("id"));
					index(jsonDoc);
					commitCount++;
					if(commitCount>300){
						solr.commit();
						commitCount=0;
					}
				}
				catch(Exception e){
					System.out.println("Error with reading and indexing json object"+e.getMessage());
				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	//Main function
	public static void main(String[] args) throws MalformedURLException, SolrServerException 
	{	
		if(args.length<3){
			System.out.println("Program execution: Java ContentAnalysis.java <path to gunTypeFile> <path to gunModelFile> <path to the downloaded index JSON file from solr>");
			System.exit(0);
		}
		ContentAnalysis ca = new ContentAnalysis(args[0].toString(),args[1].toString());
		File file = new File(args[2].toString());
		ca.getJsonNerData(file);
	}
}